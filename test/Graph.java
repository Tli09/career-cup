public void DFSearch(Node root){
	if(root == null) return;
	if(root.isVisited == true) return;
	System.out.println(root.val);
	root.isVisited = true;
	
	for(Node node : root.adjacents){
		DFSearch(node);
	}
}

public void BFSearch(Node root){
	if(root == null ) return;
	Queue<Node> q = new Queue();
	q.add(root);
	
	while(!q.isEmpty()){
		Node node1 = q.pop();
		node1.isVisited = true;
		for(Node n : node1.adjacents){
			if(n.isVisited == false) 
				q.push(n);
		}
	}
}